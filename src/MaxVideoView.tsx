import React, {useContext} from 'react';
import {RtcLocalView, RtcRemoteView, VideoRenderMode} from 'react-native-agora';
import { View } from 'react-native';
import styles from './Style';
import PropsContext from './PropsContext';
import {UidInterface} from './RtcContext';

const LocalView = RtcLocalView.SurfaceView;
const RemoteView = RtcRemoteView.SurfaceView;

interface MaxViewInterface {
  user: UidInterface;
}

const MaxVideoView: React.FC<MaxViewInterface> = (props) => {
  const {styleProps} = useContext(PropsContext);
  const {maxViewStyles} = styleProps || {};
  
  return props.user.video ? (props.user.uid === 'local' ? (
    <LocalView
      style={{...(maxViewStyles as object)}}
      renderMode={VideoRenderMode.Hidden}
    />
  ) : (
    <>
      <RemoteView
        style={{...(maxViewStyles as object)}}
        uid={props.user.uid as number}
        renderMode={VideoRenderMode.Hidden}
      />
    </>
  )) : (<View style={{backgroundColor:'#000'}}></View>);
};

export default MaxVideoView;
