import React, { useContext } from 'react';
import { View} from 'react-native';
import { RtcLocalView, RtcRemoteView, VideoRenderMode } from 'react-native-agora';
import PropsContext from './PropsContext';
import { UidInterface } from './RtcContext';

const LocalView = RtcLocalView.SurfaceView;
const RemoteView = RtcRemoteView.SurfaceView;

interface EqualVideoView {
  user: UidInterface;
  color?: string;
}

const EqualVideoView: React.FC<EqualVideoView> = (props) => {
  const { styleProps } = useContext(PropsContext);
  const { equalGridStyles } = styleProps || {};
  const {isTablet} = props;
  const gridStyle = isTablet ? equalGridStyles[props.numParticipants] : equalGridStyles[props.numParticipants]
  
  return (
    <View style={{ ...(gridStyle as object) }}>
      {props.user.video ? (<View>
        {props.user.uid === 'local' ? (<LocalView
        style={{height:'100%', width:'100%'}}
            renderMode={VideoRenderMode.Hidden}
            zOrderMediaOverlay={true}
          /> ) : (<RemoteView
            style={{height:'100%', width:'100%'}}
            uid={props.user.uid as number}
            renderMode={VideoRenderMode.Hidden}
            zOrderMediaOverlay={true}
          />)}
      </View>) : (<View style={{backgroundColor:'#000'}}></View>)}
    </View>
  );
};

export default EqualVideoView;
