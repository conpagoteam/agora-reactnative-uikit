import { Dimensions, Platform } from 'react-native';

const { width: DEVICE_WIDTH, height: DEVICE_HEIGHT } = Dimensions.get('window');

//TODO: Size of the android footer, need to check the ios one
const footerSize = Platform.OS === 'android' ? 25 : 0;

const HEIGHT_1_TO_2 = DEVICE_HEIGHT - footerSize;
const HEIGHT_3_TO_8 = DEVICE_HEIGHT / 2 - footerSize / 2;
const HEIGHT_9_TO_18 = DEVICE_HEIGHT / 3 - footerSize / 3;

const WIDTH_2_TO_4 = DEVICE_WIDTH / 2;
const WIDTH_5_6_9 = DEVICE_WIDTH / 3;
const WIDTH_7_8_10_11_12 = DEVICE_WIDTH / 4;
const WIDTH_13_TO_15 = DEVICE_WIDTH / 5;
const WIDTH_16_TO_18 = DEVICE_WIDTH / 6;

const FLEXBASIS_2_TO_4 = '50%';
const FLEXBASIS_5_6_9 = '33.3%';
const FLEXBASIS_7_8_10_11_12 = '25%';
const FLEXBASIS_13_TO_15 = '20%';
const FLEXBASIS_16_TO_18 = '16.6%';
export const roomGridTablet = {
  1: {
    width: '100%',
    height: HEIGHT_1_TO_2,
    flexBasis: '100%',
  },
  2: {
    width: WIDTH_2_TO_4,
    height: HEIGHT_1_TO_2,
    flexBasis: FLEXBASIS_2_TO_4,
  },
  3: {
    width: WIDTH_2_TO_4,
    height: HEIGHT_3_TO_8,
    flexBasis: FLEXBASIS_2_TO_4,
  },
  4: {
    width: WIDTH_2_TO_4,
    height: HEIGHT_3_TO_8,
    flexBasis: FLEXBASIS_2_TO_4,
  },
  5: {
    width: WIDTH_5_6_9,
    height: HEIGHT_3_TO_8,
    flexBasis: FLEXBASIS_5_6_9,
  },
  6: {
    width: WIDTH_5_6_9,
    height: HEIGHT_3_TO_8,
    flexBasis: FLEXBASIS_5_6_9,
  },
  7: {
    width: WIDTH_7_8_10_11_12,
    height: HEIGHT_3_TO_8,
    flexBasis: FLEXBASIS_7_8_10_11_12,
  },
  8: {
    width: WIDTH_7_8_10_11_12,
    height: HEIGHT_3_TO_8,
    flexBasis: FLEXBASIS_7_8_10_11_12,
  },
  9: {
    width: WIDTH_5_6_9,
    height: HEIGHT_9_TO_18,
    flexBasis: FLEXBASIS_5_6_9,
  },
  10: {
    width: WIDTH_7_8_10_11_12,
    height: HEIGHT_9_TO_18,
    flexBasis: FLEXBASIS_7_8_10_11_12,
  },
  11: {
    width: WIDTH_7_8_10_11_12,
    height: HEIGHT_9_TO_18,
    flexBasis: FLEXBASIS_7_8_10_11_12,
  },
  12: {
    width: WIDTH_7_8_10_11_12,
    height: HEIGHT_9_TO_18,
    flexBasis: FLEXBASIS_7_8_10_11_12,
  },
  13: {
    width: WIDTH_13_TO_15,
    height: HEIGHT_9_TO_18,
    flexBasis: FLEXBASIS_13_TO_15,
  },
  14: {
    width: WIDTH_13_TO_15,
    height: HEIGHT_9_TO_18,
    flexBasis: FLEXBASIS_13_TO_15,
  },
  15: {
    width: WIDTH_13_TO_15,
    height: HEIGHT_9_TO_18,
    flexBasis: FLEXBASIS_13_TO_15,
  },
  16: {
    width: WIDTH_16_TO_18,
    height: HEIGHT_9_TO_18,
    flexBasis: FLEXBASIS_16_TO_18,
  },
  17: {
    width: WIDTH_16_TO_18,
    height: HEIGHT_9_TO_18,
    flexBasis: FLEXBASIS_16_TO_18,
  },
  18: {
    width: WIDTH_16_TO_18,
    height: HEIGHT_9_TO_18,
    flexBasis: FLEXBASIS_16_TO_18,
  },
};

export const roomGridMobile = {
  1: {
    width: '100%',
    height: '100%',
    flexBasis: '100%',
  },
  2: {
    width: '100%',
    height: '50%',
    flexBasis: '100%',
  },
  3: {
    width: '100%',
    height: '33.3%',
    flexBasis: '100%',
  },
  4: {
    width: '50%',
    height: '50%',
    flexBasis: '50%',
  },
  5: {
    width: '50%',
    height: '33.3%',
    flexBasis: '50%',
  },
  6: {
    width: '50%',
    height: '33.3%',
    flexBasis: '50%',
  },
  7: {
    width: '50%',
    height: '25%',
    flexBasis: '50%',
  },
  8: {
    width: '50%',
    height: '25%',
    flexBasis: '50%',
  },
  9: {
    width: '50%',
    height: '20%',
    flexBasis: '50%',
  },
  10: {
    width: '50%',
    height: '20%',
    flexBasis: '50%',
  },
  11: {
    width: '33.3%',
    height: '25%',
    flexBasis: '33.3%',
  },
  12: {
    width: '33.3%',
    height: '25%',
    flexBasis: '33.3%',
  },
  13: {
    width: '33.3%',
    height: '20%',
    flexBasis: '33.3%',
  },
  14: {
    width: '33.3%',
    height: '20%',
    flexBasis: '33.3%',
  },
  15: {
    width: '33.3%',
    height: '20%',
    flexBasis: '33.3%',
  },
  16: {
    width: '25%',
    height: '25%',
    flexBasis: '25%',
  },
  17: {
    width: '25%',
    height: '20%',
    flexBasis: '25%',
  },
  18: {
    width: '25%',
    height: '20%',
    flexBasis: '25%',
  },
  19: {
    width: '25%',
    height: '20%',
    flexBasis: '25%',
  },
  20: {
    width: '25%',
    height: '20%',
    flexBasis: '25%',
  },
};
