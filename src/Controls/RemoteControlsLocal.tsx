import React, {useContext} from 'react';
import {View} from 'react-native';
import PropsContext from '../PropsContext';
import {UidInterface} from '../RtcContext';
import styles from '../Style';
import RemoteAudioMuteSingle from './Remote/RemoteAudioMuteSingle';

interface RemoteControlsInterface {
  showMuteRemoteVideo?: boolean;
  showMuteRemoteAudio?: boolean;
  showRemoteSwap?: boolean;
  user: UidInterface;
}

const RemoteControls: React.FC<RemoteControlsInterface> = (props) => {
  const {styleProps} = useContext(PropsContext);
  const {remoteBtnContainer} = styleProps || {};

  return (
    <View
      style={{height:'100%', width:'100%', justifyContent:'center', alignItems:'center'}}>
      {props.showMuteRemoteAudio !== false ? (
        <RemoteAudioMuteSingle user={props.user} />
      ) : (
        <></>
      )}
    </View>
  );
};

export default RemoteControls;
