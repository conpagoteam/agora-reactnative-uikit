import React, {useContext} from 'react';
import PropsContext from '../../PropsContext';
import RtcContext, {DispatchType} from '../../RtcContext';
import BtnTemplate from '../BtnTemplate';
import styles from '../../Style';

function ChangeView({onChangeView, minMaxView}) {
  const {styleProps} = useContext(PropsContext);
  const {localBtnStyles} = styleProps || {};
  const {changeView} = localBtnStyles || {};
  const {dispatch} = useContext(RtcContext);
  return (
    <BtnTemplate
      name={minMaxView ? 'gridView' : 'fullscreen'}
      style={{...styles.localBtn, ...(changeView as object)}}
      onPress={() => onChangeView()}
    />
  );
}

export default ChangeView;
