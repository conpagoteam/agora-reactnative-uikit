import React, {useContext} from 'react';
import {View} from 'react-native';
import styles from '../Style';
import EndCall from './Local/EndCall';
import LocalAudioMute from './Local/LocalAudioMute';
import LocalVideoMute from './Local/LocalVideoMute';
import SwitchCamera from './Local/SwitchCamera';
import ChangeView from './Local/ChangeView';
import RemoteControlsLocal from './RemoteControlsLocal';
import {MaxUidConsumer} from '../MaxUidContext';
import PropsContext from '../PropsContext';
import LocalUserContextComponent from '../LocalUserContext';

function Controls({isLive, onChangeView, minMaxView, showMuteRemoteBtn}) {
  const {styleProps} = useContext(PropsContext);
  const {localBtnContainer} = styleProps || {};

  return (
    <LocalUserContextComponent>
      <View style={{...(localBtnContainer as object) }}>
        {isLive && <EndCall />}
        {!isLive && (<><LocalAudioMute />
        <LocalVideoMute />
        <EndCall />
        <SwitchCamera />
        <ChangeView onChangeView={onChangeView} minMaxView={minMaxView}/></>)}
      </View>
      {showMuteRemoteBtn && minMaxView && (<MaxUidConsumer>
        {(users) => (
          <View
            style={{...styles.Controls, bottom: styles.Controls.bottom + 70}}>
            <RemoteControlsLocal user={users[0]} showRemoteSwap={false} />
          </View>
        )}
      </MaxUidConsumer>)}
    </LocalUserContextComponent>
  );
}

export default Controls;
