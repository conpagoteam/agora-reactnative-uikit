import React, { useState } from 'react';
import { ScrollView, View, Text, TouchableOpacity } from 'react-native';
import RtcConfigure from './RTCConfigure';
import MaxVideoView from './MaxVideoView';
import MinVideoView from './MinVideoView';
import minState, { MinUidConsumer } from './MinUidContext';
import { MaxUidConsumer } from './MaxUidContext';
import { PropsProvider, PropsInterface } from './PropsContext';
import EqualVideoView from './EqualVideoView';
import styles from './Style';
import LocalControls from './Controls/LocalControls';
import { verticalScale } from 'react-native-size-matters';

const AgoraUIKit: React.FC<PropsInterface> = (props) => {
  const { rtcProps, messages } = props;
  const [minMaxView, setMinMaxView] = useState(false);
  const handleChangeView = () => {
    setMinMaxView(!minMaxView);
  };

  const [isShowingControls, setIsShowingControls] = useState(true);

  const onToggleControlView = () => {
    setIsShowingControls(!isShowingControls);
  };

  return (
    <PropsProvider value={props}>
      <TouchableOpacity onPress={onToggleControlView}>
        <RtcConfigure>
          <View
            style={{
              height: '100%',
              width: '100%',
              backgroundColor: 'black',
              flexDirection: 'row',
              flexWrap: 'wrap',
              justifyContent: 'center',
            }}>
            <MaxUidConsumer>
              {(maxUsers) => (
                <MinUidConsumer>
                  {(minUsers) => (
                    <>
                      {/* When its NOT a livestream and user is 
                      waiting for other users to join the call */}
                      {!rtcProps.isLive &&
                        maxUsers?.length + minUsers?.length === 1 && (
                          <View
                            style={{
                              position: 'absolute',
                              top: 30,
                              zIndex: 10,
                              width: '100%',
                              justifyContent: 'center',
                              alignItems: 'center',
                            }}>
                            <Text
                              style={{
                                backgroundColor: 'rgba(0,0,0,0.7)',
                                color: 'white',
                                padding: 15,
                                borderRadius: 10,
                                fontSize: verticalScale(16),
                              }}>
                              {messages?.waitingOtherUsers}
                            </Text>
                          </View>
                        )}
                      {rtcProps.isLive &&
                        minUsers?.length === 0 && (
                          <View
                            style={{
                              position: 'absolute',
                              top: '48%',
                              zIndex: 10,
                              width: '100%',
                              justifyContent: 'center',
                              alignItems: 'center',
                            }}>
                            <Text
                              style={{
                                backgroundColor: 'rgba(0,0,0,0.7)',
                                color: 'white',
                                padding: 15,
                                borderRadius: 10,
                                fontSize: verticalScale(18),
                              }}>
                              {messages?.waitingPresenter}
                            </Text>
                          </View>
                        )}
                      {minMaxView || rtcProps.isLive ? (
                        <>
                          <MaxVideoView
                            user={maxUsers[0]}
                            key={maxUsers[0]?.uid}
                          />
                          {!rtcProps.isLive && (
                            <ScrollView
                              showsHorizontalScrollIndicator={true}
                              horizontal={true}
                              style={styles.minContainer}>
                              {minUsers.map((user) => (
                                <MinVideoView
                                  user={user}
                                  key={user.uid}
                                  showMuteRemoteBtn={rtcProps.showMuteRemoteBtn}
                                />
                              ))}
                            </ScrollView>
                          )}
                        </>
                      ) : (
                        [...minUsers, ...maxUsers].map((user) => (
                          <EqualVideoView
                            showMuteRemoteBtn={rtcProps.showMuteRemoteBtn}
                            isTablet={rtcProps.isTablet}
                            user={user}
                            key={user?.uid}
                            numParticipants={maxUsers?.length + minUsers?.length}
                          />
                        ))
                      )}
                    </>
                  )}
                </MinUidConsumer>
              )}
            </MaxUidConsumer>
            {isShowingControls && (
              <LocalControls
                showMuteRemoteBtn={rtcProps.showMuteRemoteBtn}
                isLive={rtcProps.isLive}
                onChangeView={handleChangeView}
                minMaxView={minMaxView}
              />
            )}
          </View>
        </RtcConfigure>
      </TouchableOpacity>
    </PropsProvider>
  );
};

export default AgoraUIKit;
